package com.example.rctexample.business.base

sealed class ResultBase<out T : Any> {
    data class Success<out T : Any>(val data: T) : ResultBase<T>()
    data class Error(val errorCode: Int, val errorMessage: String) : ResultBase<Nothing>()
}