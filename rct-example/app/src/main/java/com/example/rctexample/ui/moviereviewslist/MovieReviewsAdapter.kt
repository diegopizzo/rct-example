package com.example.rctexample.ui.moviereviewslist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.rctexample.R
import com.example.rctexample.business.dataviewmodel.MovieReview
import kotlinx.android.synthetic.main.item_movie_review_layout.view.*

class MovieReviewsAdapter(private val context: Context) :
    PagedListAdapter<MovieReview, MovieReviewsAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = inflater.inflate(R.layout.item_movie_review_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movieReview = getItem(position)
        movieReview?.let {
            holder.movieReviewTitle.text = it.title
            holder.movieReviewSummary.text = it.summary
            holder.movieReviewDateUpdated.text = it.dateUpdated
            Glide.with(context).load(movieReview.imgUrl).into(holder.movieReviewImage)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val movieReviewImage: ImageView = itemView.iv_movie_reviews
        val movieReviewTitle: TextView = itemView.tv_title
        val movieReviewSummary: TextView = itemView.tv_summary
        val movieReviewDateUpdated: TextView = itemView.tv_dateUpdated
    }

    companion object {
        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         */
        private val diffCallback = object : DiffUtil.ItemCallback<MovieReview>() {
            override fun areItemsTheSame(oldItem: MovieReview, newItem: MovieReview): Boolean =
                oldItem.hashCode() == newItem.hashCode()

            override fun areContentsTheSame(oldItem: MovieReview, newItem: MovieReview): Boolean =
                oldItem == newItem
        }
    }

}