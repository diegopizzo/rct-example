package com.example.rctexample.ui.moviereviewsfab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rctexample.R
import com.example.rctexample.databinding.FragmentMovieReviewsFabBinding
import com.example.rctexample.ui.helper.SnackBarCreator
import com.example.rctexample.ui.model.ScreenStateFab
import org.koin.android.viewmodel.ext.android.viewModel

class MovieReviewsFabFragment : Fragment() {
    private val viewModel: MovieReviewsViewModelCoroutines by viewModel()
    private lateinit var binding: FragmentMovieReviewsFabBinding
    private lateinit var movieReviewsAdapter: MoviesReviewsAdapterFab
    private var pageCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieReviewsAdapter = MoviesReviewsAdapterFab(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_reviews_fab, container, false)
        binding.viewModel = viewModel
        setRecyclerView()
        renderView()
        onFabClick()
        return binding.coordinatorLayout
    }

    private fun setRecyclerView() {
        binding.rvMovieReviewsFab.apply {
            adapter = movieReviewsAdapter
            layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun renderView() {
        viewModel.screenState.observe(this, Observer { screenStateValue ->
            when (screenStateValue) {
                is ScreenStateFab.MovieReviewsList -> {
                    movieReviewsAdapter.addDatas(screenStateValue.list.moviesReviews)
                    binding.tvEmptyList.visibility = View.GONE
                }
                ScreenStateFab.ProgressBarShow -> {
                    binding.tvEmptyList.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                }
                ScreenStateFab.ProgressBarHide -> binding.progressBar.visibility = View.GONE
                ScreenStateFab.EmptyResult -> binding.progressBar.visibility = View.VISIBLE
                ScreenStateFab.InternalError -> SnackBarCreator.showServerErrorMessage(binding.coordinatorLayout, context!!)
                ScreenStateFab.ExternalError -> SnackBarCreator.showNetworkErrorMessage(binding.coordinatorLayout, context!!)
            }
        })
    }

    private fun onFabClick(){
        binding.floatingActionButton.setOnClickListener {
            viewModel.getMovieReviewsList(pageCount)
            pageCount += 20
        }
    }

    companion object {
        const val MOVIE_REVIEWS_FAB_FRAGMENT_TAG = "movieReviewsFragmentFabTag"

        fun newInstance(bundle: Bundle?): MovieReviewsFabFragment {
            val movieReviewsFabFragment = MovieReviewsFabFragment()
            if (bundle != null) {
                movieReviewsFabFragment.arguments = bundle
            }
            return movieReviewsFabFragment
        }
    }
}