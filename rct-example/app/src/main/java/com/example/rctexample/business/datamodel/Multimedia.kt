package com.example.rctexample.business.datamodel

import com.google.gson.annotations.Expose

data class Multimedia(@Expose val src: String, @Expose val width: Int, @Expose val height: Int)