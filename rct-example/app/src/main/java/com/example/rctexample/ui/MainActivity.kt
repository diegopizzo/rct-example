package com.example.rctexample.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.rctexample.R
import com.example.rctexample.config.RCTExampleApplication
import com.example.rctexample.ui.moviereviewsfab.MovieReviewsFabFragment
import com.example.rctexample.ui.moviereviewslist.MovieReviewsFragment
import com.example.rctexample.ui.moviereviewslist.MovieReviewsFragmentSealed
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        //startMovieReviewsFragment()
        startMovieReviewsFabFragment()
    }

    private fun startMovieReviewsFragment() {
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(MovieReviewsFragmentSealed.MOVIE_REVIEWS_FRAGMENT_TAG)
        if (fragment == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                MovieReviewsFragmentSealed.newInstance(null),
                MovieReviewsFragmentSealed.MOVIE_REVIEWS_FRAGMENT_TAG
            ).commit()
        }
    }

    private fun startMovieReviewsFabFragment() {
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(MovieReviewsFabFragment.MOVIE_REVIEWS_FAB_FRAGMENT_TAG)
        if (fragment == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                MovieReviewsFabFragment.newInstance(null),
                MovieReviewsFabFragment.MOVIE_REVIEWS_FAB_FRAGMENT_TAG
            ).commit()
        }
    }
}