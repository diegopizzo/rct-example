package com.example.rctexample.config.koin

import com.example.rctexample.ui.moviereviewsfab.MovieReviewsViewModelCoroutines
import com.example.rctexample.ui.moviereviewslist.MovieReviewsViewModelSealed
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { MovieReviewsViewModelSealed(get()) }
    viewModel { MovieReviewsViewModelCoroutines(get()) }
}