package com.example.rctexample.config.koin

import com.example.rctexample.business.creator.MovieReviewsCreator
import com.example.rctexample.business.interactor.IMovieReviewsCoroutinesInteractor
import com.example.rctexample.business.interactor.MovieReviewsCoroutinesInteractor
import com.example.rctexample.business.interactor.MovieReviewsInteractor
import com.example.rctexample.business.network.cache.MovieReviewsStore
import com.example.rctexample.business.network.service.MovieReviewsService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { providesRetrofit() }
    single { providesMovieReviewsService(get()) }
    single { providesMovieReviewsStore(get()) }
    single { providesMovieReviewsInteractor(get(), get()) }
    single { providesMovieReviewsCoroutinesInteractor(get(), get()) }

}

fun providesRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(MovieReviewsService.ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun providesMovieReviewsService(retrofit: Retrofit): MovieReviewsService {
    return retrofit.create(MovieReviewsService::class.java)
}

fun providesMovieReviewsStore(movieReviewsService: MovieReviewsService): MovieReviewsStore {
    return MovieReviewsStore(movieReviewsService)
}

fun providesMovieReviewsInteractor(
    movieReviewsStore: MovieReviewsStore,
    creator: MovieReviewsCreator
): MovieReviewsInteractor {
    return MovieReviewsInteractor(movieReviewsStore, creator)
}

fun providesMovieReviewsCoroutinesInteractor(
    movieReviewsService: MovieReviewsService,
    creator: MovieReviewsCreator
): IMovieReviewsCoroutinesInteractor {
    return MovieReviewsCoroutinesInteractor(movieReviewsService, creator)
}
