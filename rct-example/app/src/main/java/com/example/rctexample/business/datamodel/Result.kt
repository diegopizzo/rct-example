package com.example.rctexample.business.datamodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Result(
    @Expose val display_title: String,
    @Expose @SerializedName("summary_short") val summaryShort: String,
    @Expose @SerializedName("date_updated") val dateUpdated: String?,
    @Expose val multimedia: Multimedia?
)