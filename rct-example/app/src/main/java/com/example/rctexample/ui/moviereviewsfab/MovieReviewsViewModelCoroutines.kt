package com.example.rctexample.ui.moviereviewsfab

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rctexample.business.base.ErrorType
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import com.example.rctexample.business.interactor.IMovieReviewsCoroutinesInteractor
import com.example.rctexample.ui.model.ScreenStateFab
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MovieReviewsViewModelCoroutines(private val interactor: IMovieReviewsCoroutinesInteractor) :
    ViewModel(), CoroutineScope {

    private val screenStateMutable: MutableLiveData<ScreenStateFab> = MutableLiveData()
    val screenState: LiveData<ScreenStateFab> get() = screenStateMutable

    private val job = Job()

    override val coroutineContext: CoroutineContext get() = Dispatchers.Main + job

    fun getMovieReviewsList(offset: Int) {
        launch {
            //Show progress bar (MAIN)
            showProgressBar()
            //Fetch data (IO)
            val result = withContext(Dispatchers.IO) {
                interactor.getMovieReviewsList(offset)
            }
            //Show list or empty state or error state (MAIN)
            showResults(result)
            //Hide progress bar (MAIN)
            hideProgressBar()
        }
    }

    private fun showProgressBar() {
        screenStateMutable.value = ScreenStateFab.ProgressBarShow
    }

    private fun hideProgressBar() {
        screenStateMutable.value = ScreenStateFab.ProgressBarHide
    }

    private fun showResults(result: MovieReviewsBase) {
        when {
            result.error != null ->
                when (result.error) {
                    ErrorType.NETWORK_ERROR -> screenStateMutable.value =
                        ScreenStateFab.ExternalError
                    ErrorType.CLIENT_ERROR, ErrorType.SERVER_ERROR -> screenStateMutable.value =
                        ScreenStateFab.InternalError
                }
            result.numResults == 0 -> screenStateMutable.value = ScreenStateFab.EmptyResult
            else -> screenStateMutable.value = ScreenStateFab.MovieReviewsList(result)
        }
    }

    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }

    /**
     *
     * Parallel calling
     *
     * suspend fun fetchDataInParallel() =
     *  coroutineScope {
     *  val dataOne = async { fetchData(1) }
     *  val dataTwo = async { fetchData(2) }
     *  dataOne.await()
     *  dataTwo.await()
     * }
     *
     * suspend fun fetchDataInParallel() =
     *  coroutineScope {
     *  val dataList = listOf(
     *      async { fetchData(1) },
     *      async { fetchData(2) }
     * )
     *  dataList.awaitAll()
     * }
     */
}