package com.example.rctexample.business.base

import android.util.Log
import retrofit2.Response

open class BaseInteractorService {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): ResultBase<T>? {
        return try {
            val result: ResultBase<T> = safeApiResult(call)
            when (result) {
                is ResultBase.Success -> ResultBase.Success(result.data)
                is ResultBase.Error -> {
                    Log.d("Error", "${result.errorCode} - ${result.errorMessage}")
                    ResultBase.Error(result.errorCode, result.errorMessage)
                }
            }
        } catch (e: Exception) {
            //Typically this happen when there is no connection
            Log.d("Exception", e.message)
            null
        }
    }

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): ResultBase<T> {
        val response = call.invoke()
        if (response.isSuccessful) return ResultBase.Success(response.body()!!)
        return ResultBase.Error(response.code(), response.message())
    }
}