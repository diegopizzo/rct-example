package com.example.rctexample.business.pagingdatasource

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.interactor.MovieReviewsInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MovieReviewsDataSource(private val interactor: MovieReviewsInteractor) : PageKeyedDataSource<Int, MovieReview>() {

    private val compositeDisposable = CompositeDisposable()
    private val progressBarVisibilityStateMutable: MutableLiveData<Int> = MutableLiveData()
    val progressBarVisibilityState: LiveData<Int> get() = progressBarVisibilityStateMutable

    private val shimmerLoadingVisibilityStateMutable: MutableLiveData<Int> = MutableLiveData()
    val shimmerLoadingVisibilityState: LiveData<Int> get() = shimmerLoadingVisibilityStateMutable

    private val errorStateMutable: MutableLiveData<Boolean> = MutableLiveData(false)
    val isErrorState: LiveData<Boolean> get() = errorStateMutable

    private val onSwipeToRefreshStateMutable: MutableLiveData<Boolean> = MutableLiveData()
    val onSwipeToRefreshState: LiveData<Boolean> get() = onSwipeToRefreshStateMutable

    fun stopSwipeLayout() {
        onSwipeToRefreshStateMutable.postValue(false)
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MovieReview>) {
        val singleMovieReviews = interactor.getMoviesReviews(0)
        compositeDisposable.add(
            singleMovieReviews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onInitialStart() }
                .doAfterSuccess {onInitialEnd()}
                .subscribe({ movieReviewsBase ->
                    callback.onResult(movieReviewsBase.moviesReviews, null, movieReviewsBase.numResults)
                }, {
                    onInitialError()
                })
        )
    }

    private fun onInitialStart() {
        updateLoadingShimmerVisibility(VISIBLE)
    }

    private fun onInitialEnd() {
        if (errorStateMutable.value == false) updateLoadingShimmerVisibility(GONE)
    }

    private fun onInitialError() {
        errorStateMutable.value = true
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MovieReview>) {
        val singleMovieReviews = interactor.getMoviesReviews(params.key)
        compositeDisposable.add(
            singleMovieReviews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onAfterStart(params.key) }
                .doAfterSuccess { onAfterEnd() }
                .subscribe({ movieReviewsBase ->
                    callback.onResult(movieReviewsBase.moviesReviews, params.key + 20)
                }, { onAfterError() })
        )
    }

    private fun onAfterStart(key: Int) {
        if (key != FIRST_PAGE) updateProgressBarVisibility(VISIBLE)
    }

    private fun onAfterEnd() {
        if (errorStateMutable.value != true) updateProgressBarVisibility(GONE)
    }

    private fun onAfterError() {
        updateProgressBarVisibility(VISIBLE)
        errorStateMutable.postValue(true)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MovieReview>) {
    }

    private fun updateProgressBarVisibility(visibilityValue: Int) {
        this.progressBarVisibilityStateMutable.postValue(visibilityValue)
    }

    private fun updateLoadingShimmerVisibility(visibilityValue: Int) {
        shimmerLoadingVisibilityStateMutable.postValue(visibilityValue)
    }

    fun clearDisposables() {
        compositeDisposable.clear()
    }

    companion object {
        private const val FIRST_PAGE = 20
    }

    class MovieReviewsDataSourceFactory(private val dataSource: MovieReviewsDataSource) :
        DataSource.Factory<Int, MovieReview>() {

        val movieReviewsDataSource = MutableLiveData<MovieReviewsDataSource>()

        override fun create(): DataSource<Int, MovieReview> {
            movieReviewsDataSource.postValue(dataSource)
            return dataSource
        }
    }
}