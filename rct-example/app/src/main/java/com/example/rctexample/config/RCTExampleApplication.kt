package com.example.rctexample.config

import android.app.Application
import com.example.rctexample.config.koin.networkModule
import com.example.rctexample.config.koin.pagingDataSourceModule
import com.example.rctexample.config.koin.utilsModule
import com.example.rctexample.config.koin.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class RCTExampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@RCTExampleApplication)
            modules(listOf(networkModule, utilsModule, viewModelsModule, pagingDataSourceModule))
        }
    }
}