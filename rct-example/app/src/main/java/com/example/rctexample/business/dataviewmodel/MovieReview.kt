package com.example.rctexample.business.dataviewmodel

data class MovieReview(
    val title: String,
    val summary: String,
    val dateUpdated: String,
    val imgUrl: String?,
    val imgWidth: Int?,
    val imgHeight: Int?
)