package com.example.rctexample.business.interactor

import com.example.rctexample.business.dataviewmodel.MovieReviewsBase

interface IMovieReviewsCoroutinesInteractor {

    suspend fun getMovieReviewsList(offset: Int): MovieReviewsBase
}