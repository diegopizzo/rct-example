package com.example.rctexample.ui.model

import com.example.rctexample.business.dataviewmodel.MovieReviewsBase

sealed class ScreenStateFab {
    class MovieReviewsList(val list: MovieReviewsBase) : ScreenStateFab()
    object ProgressBarShow : ScreenStateFab()
    object ProgressBarHide : ScreenStateFab()
    object EmptyResult : ScreenStateFab()
    object InternalError : ScreenStateFab()
    object ExternalError : ScreenStateFab()
}