package com.example.rctexample.ui.model

sealed class ScreenState {
    object ShimmerLoadingShow : ScreenState()
    object ShimmerLoadingHide : ScreenState()
    object ProgressBarShow : ScreenState()
    object ProgressBarHide : ScreenState()
    class Error(val errorType: ErrorType) : ScreenState()
    object SwipeUpLoadingShow : ScreenState()
    object SwipeUpLoadingHide : ScreenState()
}

enum class ErrorType {
    SERVER_ERROR, NETWORK_ERROR
}