package com.example.rctexample.business.base

enum class ErrorType {
    NETWORK_ERROR, CLIENT_ERROR, SERVER_ERROR
}