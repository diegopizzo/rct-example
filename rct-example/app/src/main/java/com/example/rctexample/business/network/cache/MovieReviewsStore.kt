package com.example.rctexample.business.network.cache

import com.example.rctexample.business.datamodel.ResponseModel
import com.example.rctexample.business.network.service.MovieReviewsService
import com.nytimes.android.external.store3.base.impl.Store
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import io.reactivex.Single

class MovieReviewsStore(private val movieReviewsService: MovieReviewsService) {

    companion object {
        const val DELIMITER = ";"
    }

    private val moviesReviewsStore: Store<ResponseModel, String>

    init {
        moviesReviewsStore = StoreBuilder.key<String, ResponseModel>()
            .fetcher { key ->
                movieReviewsService.getMovieReviews(
                    key.split(DELIMITER)[0],
                    key.split(DELIMITER)[1].toInt()
                )
            }.open()
    }

    fun getMoviesReviewsFromStore(key: String, forceRefresh: Boolean = false): Single<ResponseModel> {
        return if (forceRefresh) moviesReviewsStore.fetch(key) else moviesReviewsStore.get(key)
    }
}