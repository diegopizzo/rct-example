package com.example.rctexample.ui.moviereviewslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.rctexample.R
import com.example.rctexample.databinding.FragmentMovieReviewsLayoutBinding
import com.example.rctexample.ui.helper.SnackBarCreator
import org.koin.android.viewmodel.ext.android.viewModel

class MovieReviewsFragment : Fragment() {

    private val viewModel: MovieReviewsViewModel by viewModel()
    private lateinit var binding: FragmentMovieReviewsLayoutBinding
    private lateinit var movieReviewsAdapter: MovieReviewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieReviewsAdapter = MovieReviewsAdapter(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_reviews_layout, container, false)
        //binding.viewModel = viewModel
        setRecyclerView()
        observeMovieReviewsList()
        observeProgressBarVisibility()
        observeShimmerLoadingState()
        observeSnackBarState()
        setRefreshListener()
        observeOnSwipeToRefresh()
        return binding.clMovieReviewsLayout
    }

    private fun setRecyclerView() {
        binding.rvMovieReviews.apply {
            adapter = movieReviewsAdapter
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
    }

    private fun observeMovieReviewsList() {
        viewModel.movieReviewsList.observe(this, Observer { movieReviewsAdapter.submitList(it) })
    }

    private fun observeProgressBarVisibility() {
        viewModel.progressBarVisibility.observe(
            this,
            Observer { visibility ->
                if (visibility == VISIBLE) binding.pbMovieReviews.visibility =
                    VISIBLE else binding.pbMovieReviews.visibility = GONE
            })
    }

    private fun setRefreshListener() {
        binding.slMovieReviewsRefresh.setOnRefreshListener {
            movieReviewsAdapter.notifyDataSetChanged()
            viewModel.onRefresh()
        }
    }

    private fun observeOnSwipeToRefresh() {
        viewModel.onSwipeToRefreshState.observe(this,
            Observer { isLoading -> binding.slMovieReviewsRefresh.isRefreshing = isLoading })
    }

    private fun observeShimmerLoadingState() {
        viewModel.shimmerLoadingVisibility.observe(
            this,
            Observer { state -> if (state == VISIBLE) binding.shimmerRecyclerView.showShimmerAdapter() else binding.shimmerRecyclerView.hideShimmerAdapter() })
    }

    private fun observeSnackBarState() {
        viewModel.isAnErrorOnLoading.observe(
            this,
            Observer { isError -> if (isError) SnackBarCreator.showNetworkErrorMessage(binding.clMovieReviewsLayout, context!!) }
        )
    }

    companion object {
        const val MOVIE_REVIEWS_FRAGMENT_TAG = "movieReviewsFragmentTag"

        fun newInstance(bundle: Bundle?): MovieReviewsFragment {
            val movieReviewsFragment = MovieReviewsFragment()
            if (bundle != null) {
                movieReviewsFragment.arguments = bundle
            }
            return movieReviewsFragment
        }
    }
}