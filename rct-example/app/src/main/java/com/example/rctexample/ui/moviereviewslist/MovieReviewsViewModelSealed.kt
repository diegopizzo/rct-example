package com.example.rctexample.ui.moviereviewslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSourceSealed
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSourceSealed.MovieReviewsDataSourceFactorySealed
import com.example.rctexample.config.koin.pageListConfig
import com.example.rctexample.ui.model.ScreenState

class MovieReviewsViewModelSealed(private val dataSourceFactorySealed: MovieReviewsDataSourceFactorySealed) : ViewModel() {

    val movieReviewsList: LiveData<PagedList<MovieReview>> =
        LivePagedListBuilder<Int, MovieReview>(dataSourceFactorySealed, pageListConfig).build()

    val screenState: LiveData<ScreenState> get() = screenStateCreator()

    private fun screenStateCreator() = Transformations.switchMap<MovieReviewsDataSourceSealed,
            ScreenState>(
        dataSourceFactorySealed.movieReviewsDataSource,
        MovieReviewsDataSourceSealed::screenStateMutable
    )

    fun onRefresh() {
        dataSourceFactorySealed.movieReviewsDataSource.value?.invalidate()
        dataSourceFactorySealed.movieReviewsDataSource.value?.stopSwipeLayout()
    }

    override fun onCleared() {
        dataSourceFactorySealed.movieReviewsDataSource.value?.clearDisposables()
        super.onCleared()
    }
}