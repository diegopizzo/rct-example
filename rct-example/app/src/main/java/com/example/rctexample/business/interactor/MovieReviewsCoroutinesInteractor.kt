package com.example.rctexample.business.interactor

import com.example.rctexample.business.base.BaseInteractorService
import com.example.rctexample.business.creator.MovieReviewsCreator
import com.example.rctexample.business.datamodel.ResponseModel
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import com.example.rctexample.business.network.service.MovieReviewsService
import com.example.rctexample.business.network.service.MovieReviewsService.Companion.TYPE_VALUE
import retrofit2.Response

class MovieReviewsCoroutinesInteractor(
    private val movieReviewsService: MovieReviewsService,
    private val creator: MovieReviewsCreator
) : BaseInteractorService(), IMovieReviewsCoroutinesInteractor {

    override suspend fun getMovieReviewsList(offset: Int): MovieReviewsBase {
        val result = safeApiCall(call = { getReviews(offset) })
        return creator.fromResponseToMovieReviewList(result)
    }

    private suspend fun getReviews(offset: Int): Response<ResponseModel> =
        movieReviewsService.getMovieReviewsCoroutines(TYPE_VALUE, offset)
}