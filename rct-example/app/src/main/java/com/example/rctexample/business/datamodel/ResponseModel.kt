package com.example.rctexample.business.datamodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @Expose @SerializedName("has_more") val hasMore: Boolean,
    @Expose @SerializedName("num_results") val numResults: Int,
    @Expose val results: List<Result>
)