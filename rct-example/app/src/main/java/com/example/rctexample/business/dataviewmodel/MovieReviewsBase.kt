package com.example.rctexample.business.dataviewmodel

import com.example.rctexample.business.base.ErrorType

data class MovieReviewsBase(
    val numResults: Int,
    val hasMore: Boolean,
    val moviesReviews: List<MovieReview>,
    val error: ErrorType? = null
)