package com.example.rctexample.business.creator

import com.example.rctexample.business.base.ErrorType
import com.example.rctexample.business.base.ResultBase
import com.example.rctexample.business.datamodel.ResponseModel
import com.example.rctexample.business.datamodel.Result
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class MovieReviewsCreator {

    fun fromResponseToMovieReviewList(response: ResultBase<ResponseModel>?): MovieReviewsBase {
        return when (response){
            is ResultBase.Success -> {
                MovieReviewsBase(response.data.numResults, response.data.hasMore, createMovieReviewListFromResults(response.data.results))
            }
            is ResultBase.Error -> MovieReviewsBase(0, false, listOf(), createError(response.errorCode))
            null ->  MovieReviewsBase(0, false, listOf(), ErrorType.NETWORK_ERROR)
        }
    }

    private fun createMovieReviewListFromResults(results: List<Result>): List<MovieReview> {
        return results.map { createMovieReviewFromResult(it) }
    }

    private fun createMovieReviewFromResult(result: Result): MovieReview {
        return MovieReview(
            result.display_title,
            result.summaryShort,
            formatDate(result.dateUpdated),
            result.multimedia?.src,
            result.multimedia?.width,
            result.multimedia?.height
        )
    }

    private fun formatDate(date: String?): String {
        if (date == null) return ""
        val localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(DATE_PATTERN_SOURCE, Locale.ROOT))
        return localDateTime.format(DateTimeFormatter.ofPattern(DATE_PATTERN_DESTINATION, Locale.ROOT))
    }

    private fun createError(errorCode: Int): ErrorType {
        return when {
            errorCode < 500 -> ErrorType.CLIENT_ERROR
            else -> ErrorType.SERVER_ERROR
        }
    }

    companion object {
        private const val DATE_PATTERN_SOURCE = "yyyy-MM-dd HH:mm:ss"
        private const val DATE_PATTERN_DESTINATION = "MMM d, yyyy HH:mm"
    }
}