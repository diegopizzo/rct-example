package com.example.rctexample.config.koin

import androidx.paging.PagedList
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSourceSealed
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSourceSealed.*
import org.koin.dsl.module

val pagingDataSourceModule = module {
    factory { MovieReviewsDataSourceSealed(get()) }
    factory { MovieReviewsDataSourceFactorySealed(get()) }
}

val pageListConfig = PagedList.Config.Builder()
    .setPageSize(20)
    .setInitialLoadSizeHint(20)
    .setEnablePlaceholders(false)
    .build()