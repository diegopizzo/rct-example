package com.example.rctexample.ui.helper

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.example.rctexample.R
import com.google.android.material.snackbar.Snackbar

object SnackBarCreator {

    private fun Snackbar.configureDesign(context: Context, backgroundRes: Int = R.drawable.background_snack_bar) {
        val margin = (context.resources.displayMetrics.density * 12).toInt()
        val elevation = 6F

        val params = view.layoutParams as ViewGroup.MarginLayoutParams
        params.setMargins(margin, params.topMargin, params.rightMargin + margin, params.bottomMargin + margin)
        view.layoutParams = params
        view.background = ContextCompat.getDrawable(context, backgroundRes)
        ViewCompat.setElevation(view, elevation)
    }

    fun showMessage(view: View, context: Context, messageRes: Int, titleActionRes: Int? = null, actionClickListener: View.OnClickListener? = null, duration: Int = Snackbar.LENGTH_LONG) {
        val snackBar = Snackbar.make(view, messageRes, duration)
        if (titleActionRes != null && actionClickListener != null) {
            snackBar.setAction(titleActionRes, actionClickListener)
        }
        //snackBar.configureDesign(context)
        snackBar.show()
    }

    fun showServerErrorMessage(view: View, context: Context, retryButtonClickListener: View.OnClickListener? = null) {
        showMessage(view, context, R.string.generic_error_server_problem, R.string.generic_retry, retryButtonClickListener)
    }

    fun showNetworkErrorMessage(view: View, context: Context, retryButtonClickListener: View.OnClickListener? = null) {
        showMessage(view, context, R.string.generic_error_no_network, R.string.generic_retry, retryButtonClickListener)
    }
}