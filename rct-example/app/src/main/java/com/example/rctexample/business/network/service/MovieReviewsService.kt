package com.example.rctexample.business.network.service

import com.example.rctexample.business.datamodel.ResponseModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieReviewsService {

    companion object {
        const val ENDPOINT = "https://api.nytimes.com"
        private const val API_KEY_LABEL = "api-key"
        private const val API_KEY_VALUE = "GQaKvgwkra08Gaw8Dn9W5u3w7cyGGLYf"
        private const val TYPE_LABEL = "type"
        const val TYPE_VALUE = "all"
        const val MOVIE_REVIEWS_URL_PATH = "/svc/movies/v2/reviews"
    }

    /**
     * @param type [all,picks] Specify all to retrieve all reviews, including NYT Critics' Picks.
     *                     Specify picks to get NYT Critics' Picks currently in theaters.
     * @param offset Positive integer, multiple of 20
     * @return Single<ResponseModel> observable where there is a list of movies reviews
     */

    @GET("$MOVIE_REVIEWS_URL_PATH/{$TYPE_LABEL}.json?$API_KEY_LABEL=$API_KEY_VALUE")
    fun getMovieReviews(@Path(TYPE_LABEL) type: String, @Query("offset") offset: Int): Single<ResponseModel>

    @GET("$MOVIE_REVIEWS_URL_PATH/{$TYPE_LABEL}.json?$API_KEY_LABEL=$API_KEY_VALUE")
    suspend fun getMovieReviewsCoroutines(@Path(TYPE_LABEL) type: String, @Query("offset") offset: Int): Response<ResponseModel>
}