package com.example.rctexample.config.koin

import com.example.rctexample.business.creator.MovieReviewsCreator
import org.koin.dsl.module

val utilsModule = module { factory { MovieReviewsCreator() } }