package com.example.rctexample.business.interactor

import com.example.rctexample.business.creator.MovieReviewsCreator
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import com.example.rctexample.business.network.cache.MovieReviewsStore
import com.example.rctexample.business.network.cache.MovieReviewsStore.Companion.DELIMITER
import com.example.rctexample.business.network.service.MovieReviewsService.Companion.TYPE_VALUE
import io.reactivex.Single

class MovieReviewsInteractor(
    private val movieReviewsStore: MovieReviewsStore,
    private val creator: MovieReviewsCreator) {

    fun getMoviesReviews(offset: Int?, refresh: Boolean = false): Single<MovieReviewsBase> {
        return movieReviewsStore.getMoviesReviewsFromStore(key = "$TYPE_VALUE$DELIMITER$offset",
            forceRefresh = refresh
        ).map {
            //Temporary null value, change creator function
            creator.fromResponseToMovieReviewList(null)
        }
    }
}