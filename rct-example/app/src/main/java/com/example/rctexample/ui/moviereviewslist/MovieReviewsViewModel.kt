package com.example.rctexample.ui.moviereviewslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSource
import com.example.rctexample.business.pagingdatasource.MovieReviewsDataSource.MovieReviewsDataSourceFactory
import com.example.rctexample.config.koin.pageListConfig

class MovieReviewsViewModel(private val dataSourceFactory: MovieReviewsDataSourceFactory) : ViewModel() {

    val movieReviewsList: LiveData<PagedList<MovieReview>> =
        LivePagedListBuilder<Int, MovieReview>(dataSourceFactory, pageListConfig).build()

    val shimmerLoadingVisibility: LiveData<Int> = shimmerVisibilityCreator()

    val progressBarVisibility: LiveData<Int> = progressBarVisibilityCreator()

    val isAnErrorOnLoading: LiveData<Boolean> = errorStateCreator()

    val onSwipeToRefreshState: LiveData<Boolean> = onSwipeToRefreshCreator()

    private fun progressBarVisibilityCreator(): LiveData<Int> = Transformations.switchMap<MovieReviewsDataSource,
            Int>(dataSourceFactory.movieReviewsDataSource, MovieReviewsDataSource::progressBarVisibilityState)

    private fun shimmerVisibilityCreator(): LiveData<Int> = Transformations.switchMap<MovieReviewsDataSource, Int>(
        dataSourceFactory.movieReviewsDataSource,
        MovieReviewsDataSource::shimmerLoadingVisibilityState
    )

    private fun errorStateCreator(): LiveData<Boolean> = Transformations.switchMap<MovieReviewsDataSource, Boolean>(
        dataSourceFactory.movieReviewsDataSource,
        MovieReviewsDataSource::isErrorState
    )

    private fun onSwipeToRefreshCreator(): LiveData<Boolean> =
        Transformations.switchMap<MovieReviewsDataSource, Boolean>(
            dataSourceFactory.movieReviewsDataSource,
            MovieReviewsDataSource::onSwipeToRefreshState
        )

    fun onRefresh() {
        dataSourceFactory.movieReviewsDataSource.value?.invalidate()
        dataSourceFactory.movieReviewsDataSource.value?.stopSwipeLayout()
    }

    override fun onCleared() {
        dataSourceFactory.movieReviewsDataSource.value?.clearDisposables()
        super.onCleared()
    }
}