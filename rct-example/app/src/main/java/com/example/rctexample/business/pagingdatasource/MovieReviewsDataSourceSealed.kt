package com.example.rctexample.business.pagingdatasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.interactor.MovieReviewsInteractor
import com.example.rctexample.ui.model.ErrorType
import com.example.rctexample.ui.model.ScreenState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MovieReviewsDataSourceSealed(private val interactor: MovieReviewsInteractor) :
    PageKeyedDataSource<Int, MovieReview>() {

    private val compositeDisposable = CompositeDisposable()

    val screenStateMutable: MutableLiveData<ScreenState> = MutableLiveData()

    fun stopSwipeLayout() {
        screenStateMutable.postValue(ScreenState.SwipeUpLoadingHide)
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MovieReview>) {
        val singleMovieReviews = interactor.getMoviesReviews(0)
        compositeDisposable.add(
            singleMovieReviews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onInitialStart() }
                .doAfterSuccess { onInitialEnd() }
                .subscribe({ movieReviewsBase ->
                    callback.onResult(movieReviewsBase.moviesReviews, null, movieReviewsBase.numResults)
                }, {
                    onInitialError()
                })
        )
    }

    private fun onInitialStart() {
        screenStateMutable.postValue(ScreenState.ShimmerLoadingShow)
    }

    private fun onInitialEnd() {
        screenStateMutable.postValue(ScreenState.ShimmerLoadingHide)
    }

    private fun onInitialError() {
        screenStateMutable.postValue(ScreenState.Error(ErrorType.NETWORK_ERROR))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MovieReview>) {
        val singleMovieReviews = interactor.getMoviesReviews(params.key)
        compositeDisposable.add(
            singleMovieReviews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onAfterStart(params.key) }
                .doAfterSuccess { onAfterEnd() }
                .subscribe({ movieReviewsBase ->
                    callback.onResult(movieReviewsBase.moviesReviews, params.key + 20)
                }, { onAfterError() })
        )
    }

    private fun onAfterStart(key: Int) {
        if (key != FIRST_PAGE) screenStateMutable.postValue(ScreenState.ProgressBarShow)
    }

    private fun onAfterEnd() {
        screenStateMutable.postValue(ScreenState.ProgressBarHide)
    }

    private fun onAfterError() {
        screenStateMutable.postValue(ScreenState.ProgressBarShow)
        screenStateMutable.postValue(ScreenState.Error(ErrorType.SERVER_ERROR))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MovieReview>) {
    }

    fun clearDisposables() {
        compositeDisposable.clear()
    }

    companion object {
        private const val FIRST_PAGE = 20
    }

    class MovieReviewsDataSourceFactorySealed(private val dataSource: MovieReviewsDataSourceSealed) :
        DataSource.Factory<Int, MovieReview>() {

        val movieReviewsDataSource = MutableLiveData<MovieReviewsDataSourceSealed>()

        override fun create(): DataSource<Int, MovieReview> {
            movieReviewsDataSource.postValue(dataSource)
            return dataSource
        }
    }
}
