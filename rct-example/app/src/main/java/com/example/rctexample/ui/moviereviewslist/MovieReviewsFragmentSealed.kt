package com.example.rctexample.ui.moviereviewslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.rctexample.R
import com.example.rctexample.databinding.FragmentMovieReviewsLayoutBinding
import com.example.rctexample.ui.helper.SnackBarCreator
import com.example.rctexample.ui.model.ErrorType
import com.example.rctexample.ui.model.ScreenState
import org.koin.android.viewmodel.ext.android.viewModel

class MovieReviewsFragmentSealed : Fragment() {

    private val viewModel: MovieReviewsViewModelSealed by viewModel()
    private lateinit var binding: FragmentMovieReviewsLayoutBinding
    private lateinit var movieReviewsAdapter: MovieReviewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieReviewsAdapter = MovieReviewsAdapter(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_reviews_layout, container, false)
        binding.viewModel = viewModel
        setRecyclerView()
        observeMovieReviewsList()
        setRefreshListener()
        observeScreenState()
        return binding.clMovieReviewsLayout
    }

    private fun setRecyclerView() {
        binding.rvMovieReviews.apply {
            adapter = movieReviewsAdapter
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
    }

    private fun observeMovieReviewsList() {
        viewModel.movieReviewsList.observe(this, Observer {
            movieReviewsAdapter.submitList(it)
        })
    }

    private fun observeScreenState() {
        viewModel.screenState.observe(this, Observer { renderView(it) })
    }

    private fun renderView(screenState: ScreenState) {
        when (screenState) {
            ScreenState.ShimmerLoadingShow -> binding.shimmerRecyclerView.showShimmerAdapter()
            ScreenState.ShimmerLoadingHide -> binding.shimmerRecyclerView.hideShimmerAdapter()
            ScreenState.ProgressBarShow -> binding.pbMovieReviews.visibility = VISIBLE
            ScreenState.ProgressBarHide -> binding.pbMovieReviews.visibility = GONE
            ScreenState.SwipeUpLoadingShow -> binding.slMovieReviewsRefresh.isRefreshing = true
            ScreenState.SwipeUpLoadingHide -> binding.slMovieReviewsRefresh.isRefreshing = false
            is ScreenState.Error -> {
                when (screenState.errorType) {
                    ErrorType.SERVER_ERROR -> SnackBarCreator.showServerErrorMessage(view!!, context!!)
                    ErrorType.NETWORK_ERROR -> SnackBarCreator.showNetworkErrorMessage(view!!, context!!)
                }
            }
        }
    }

    private fun setRefreshListener() {
        binding.slMovieReviewsRefresh.setOnRefreshListener {
            movieReviewsAdapter.notifyDataSetChanged()
            viewModel.onRefresh()
        }
    }

    companion object {
        const val MOVIE_REVIEWS_FRAGMENT_TAG = "movieReviewsFragmentSealedTag"

        fun newInstance(bundle: Bundle?): MovieReviewsFragmentSealed {
            val movieReviewsFragmentSealed = MovieReviewsFragmentSealed()
            if (bundle != null) {
                movieReviewsFragmentSealed.arguments = bundle
            }
            return movieReviewsFragmentSealed
        }
    }
}