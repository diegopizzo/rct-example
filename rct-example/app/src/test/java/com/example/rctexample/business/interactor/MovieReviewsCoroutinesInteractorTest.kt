package com.example.rctexample.business.interactor

import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import com.example.rctexample.testutils.CoroutinesTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class MovieReviewsCoroutinesInteractorTest {

    @Mock
    lateinit var interactor: MovieReviewsCoroutinesInteractor

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun main_test(){
        coroutinesTestRule.runBlockingTest {
            `when`(interactor.getMovieReviewsList(1)).thenReturn(movieReviewsBase)
            val result = interactor.getMovieReviewsList(1)
            assertEquals(movieReviewsBase, result)
        }
    }

    companion object {
        private val movieReviewsBase = MovieReviewsBase(1, false, listOf(MovieReview("title", "summary", "2019-04-11 02:44:20", null, null, null)))
    }
}