package com.example.rctexample.business.network.service

import com.example.rctexample.business.network.service.MovieReviewsService.Companion.MOVIE_REVIEWS_URL_PATH
import com.example.rctexample.business.network.service.MovieReviewsService.Companion.TYPE_VALUE
import io.appflate.restmock.JVMFileParser
import io.appflate.restmock.RESTMockServer
import io.appflate.restmock.RESTMockServerStarter
import io.appflate.restmock.utils.RequestMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@RunWith(JUnit4::class)
class MovieReviewsServiceTest {

    private lateinit var movieReviewsService: MovieReviewsService

    @Before
    fun setUp() {
        RESTMockServerStarter.startSync(JVMFileParser())
        val retrofit = Retrofit.Builder()
            .baseUrl(RESTMockServer.getUrl())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        movieReviewsService = retrofit.create(MovieReviewsService::class.java)
    }

    @After
    fun stopService() {
        RESTMockServer.shutdown()
    }

    @Test
    fun whenCallApiToRetrieveMovieReviewsListThenCheckIfTheResponseIsCorrect() {
        RESTMockServer.whenGET(RequestMatchers.pathContains(MOVIE_REVIEWS_URL_PATH))
            .thenReturnFile(200, RESPONSE_FILE_PATH)
        val response = movieReviewsService.getMovieReviews(TYPE_VALUE, 0).test().assertComplete()
        response.assertValue { it.numResults == 20 }.assertValue { it.hasMore }.assertValue { !it.results.isEmpty() }
            .assertValue { it.results.size == 20 }.assertValue { it.results[0].display_title == TITLE_MOVIE_RESPONSE }
    }

    companion object {
        private const val RESPONSE_FILE_PATH = "api-response/response.json"
        private const val TITLE_MOVIE_RESPONSE = "The Fate of Lee Khan"
    }
}