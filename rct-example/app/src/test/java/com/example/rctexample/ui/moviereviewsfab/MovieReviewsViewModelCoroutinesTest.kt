package com.example.rctexample.ui.moviereviewsfab

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.rctexample.business.dataviewmodel.MovieReview
import com.example.rctexample.business.dataviewmodel.MovieReviewsBase
import com.example.rctexample.business.interactor.IMovieReviewsCoroutinesInteractor
import com.example.rctexample.testutils.CoroutinesTestRule
import com.example.rctexample.ui.model.ScreenStateFab
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class MovieReviewsViewModelCoroutinesTest {

    private lateinit var viewModel: MovieReviewsViewModelCoroutines
    @Mock
    lateinit var interactor: IMovieReviewsCoroutinesInteractor
    @Mock
    lateinit var observer: Observer<ScreenStateFab>

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MovieReviewsViewModelCoroutines(interactor)
        //Attach fake observer
        viewModel.screenState.observeForever(observer)
    }

    @Test
    fun main_test() {
        coroutinesTestRule.runBlockingTest {
            `when`(interactor.getMovieReviewsList(0)).thenReturn(movieReviewsBase)
            viewModel.getMovieReviewsList(0)
            verify(observer).onChanged(ArgumentMatchers.any(ScreenStateFab.ProgressBarShow::class.java))
        }
    }

    companion object {
        private val movieReviewsBase = MovieReviewsBase(
            1,
            false,
            listOf(MovieReview("title", "summary", "2019-04-11 02:44:20", null, null, null))
        )
    }
}